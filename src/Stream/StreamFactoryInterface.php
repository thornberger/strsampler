<?php

declare(strict_types = 1);
namespace strsampler\Stream;

/**
 * A factory for a specific stream type.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
interface StreamFactoryInterface
{
    /**
     * Create a new instance of a stream.
     *
     * @param array $parameters
     *
     * @return StreamInterface
     */
    public function create(array $parameters): StreamInterface;
}