<?php

declare(strict_types = 1);
namespace strsampler\Stream\Url;

use strsampler\Stream\StreamFactoryInterface;
use strsampler\Stream\StreamInterface;

/**
 * Factory for URL streams.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStreamFactory implements StreamFactoryInterface
{
    /**
     * Creates a new URL stream.
     *
     * @param array $parameters
     *
     * @return StreamInterface
     */
    public function create(array $parameters): StreamInterface
    {
        $url = current($parameters);

        return new UrlStream($url);
    }
}