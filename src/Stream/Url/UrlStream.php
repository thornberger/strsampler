<?php

declare(strict_types = 1);
namespace strsampler\Stream\Url;

use strsampler\Stream\StreamInterface;

/**
 * Generates a stream of characters provided by data from a URL or file.
 *
 * Data is assumed to be provided in separate lines with one character in each line.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStream implements StreamInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        $input = @fopen($this->url, 'r');

        if (false === $input) {
            throw new UrlStreamException($this->url, error_get_last()['message']);
        }

        while ($line = fgets($input)) {
            $line = trim($line);
            yield $line;
        }

        fclose($input);
    }
}