<?php

declare(strict_types = 1);
namespace strsampler\Stream\Url;

use Exception;

/**
 * Thrown when trying to load streaming data from a URL or file failed (e.g. server not available, etc).
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStreamException extends Exception
{
    /**
     * @var string
     */
    private $url;

    /**
     * Creates a new exception for the given URL and message.
     *
     * @param string $url
     * @param string $message
     */
    public function __construct(string $url, string $message)
    {
        parent::__construct($message);
        $this->url = $url;
    }

    /**
     * Returns the URL.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

}