<?php

declare(strict_types = 1);
namespace strsampler\Stream;

use strsampler\Stream\Exception\UndefinedStreamTypeException;

/**
 * Creates streams for given types.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StreamFactory
{
    /**
     * @var StreamFactoryInterface[]
     */
    private $factories = [];

    /**
     * Register a stream factory.
     *
     * @param string                 $type
     * @param StreamFactoryInterface $factory
     */
    public function registerFactory(string $type, StreamFactoryInterface $factory)
    {
        $this->factories[$type] = $factory;
    }

    /**
     * Create a stream of given type.
     *
     * @param string $type
     * @param array  $parameters
     *
     * @return StreamInterface
     * @throws UndefinedStreamTypeException
     */
    public function create(string $type, array $parameters): StreamInterface
    {
        if (!isset($this->factories[$type])) {
            throw new UndefinedStreamTypeException($type);
        }

        return $this->factories[$type]->create($parameters);
    }
}