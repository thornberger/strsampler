<?php

declare(strict_types = 1);
namespace strsampler\Stream\Exception;

use Exception;

/**
 * Thrown when trying to create a stream for an undefined type.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UndefinedStreamTypeException extends Exception
{
    const MESSAGE_TEMPLATE = 'Undefined stream type "%s"';

    /**
     * Creates a new exception for the given undefined type.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {
        parent::__construct(sprintf(self::MESSAGE_TEMPLATE, $type));
    }
}