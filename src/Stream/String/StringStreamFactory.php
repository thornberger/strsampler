<?php

declare(strict_types = 1);
namespace strsampler\Stream\String;

use strsampler\Stream\StreamFactoryInterface;
use strsampler\Stream\StreamInterface;

/**
 * Factory for string streams.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StringStreamFactory implements StreamFactoryInterface
{
    /**
     * Creates a new string stream.
     *
     * @param array $parameters
     *
     * @return StreamInterface
     */
    public function create(array $parameters): StreamInterface
    {
        $string = current($parameters);

        return new StringStream($string);
    }
}