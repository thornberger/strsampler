<?php

declare(strict_types = 1);
namespace strsampler\Stream\String;

use ArrayIterator;
use strsampler\Stream\StreamInterface;

/**
 * Generates a stream of characters provided by a string.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StringStream implements StreamInterface
{
    /**
     * @var string
     */
    private $string;

    /**
     * Creates a new string stream.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        return new ArrayIterator(str_split($this->string));
    }
}