<?php

declare(strict_types = 1);
namespace strsampler\Stream\Random;

use strsampler\Common\Random;
use strsampler\Stream\StreamFactoryInterface;
use strsampler\Stream\StreamInterface;

/**
 * Factory for random streams.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class RandomStreamFactory implements StreamFactoryInterface
{
    /**
     * @var Random
     */
    private $random;

    /**
     * Creates a new factory.
     *
     * @param Random $random
     */
    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    /**
     * Creates a new random stream.
     *
     * @param array $parameters
     *
     * @return StreamInterface
     */
    public function create(array $parameters): StreamInterface
    {
        $length = (int) current($parameters);

        return new RandomStream($this->random, $length);
    }
}