<?php

declare(strict_types = 1);
namespace strsampler\Stream\Random;

use strsampler\Common\Random;
use strsampler\Stream\StreamInterface;

/**
 * Generates a stream of random bytes.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class RandomStream implements StreamInterface
{
    /**
     * @var Random
     */
    private $random;
    /**
     * @var int
     */
    private $length;

    /**
     * Creates a new random stream.
     *
     * @param Random $random
     * @param int    $length
     */
    public function __construct(Random $random, int $length)
    {
        $this->random = $random;
        $this->length = $length;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        for ($i = 0; $i < $this->length; $i++) {
            yield $this->random->getRandomByte();
        }
    }
}