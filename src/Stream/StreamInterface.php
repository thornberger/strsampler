<?php

declare(strict_types = 1);
namespace strsampler\Stream;

use IteratorAggregate;

/**
 * Provides a stream of characters.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
interface StreamInterface extends IteratorAggregate
{

}