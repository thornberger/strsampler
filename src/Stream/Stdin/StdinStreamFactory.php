<?php

declare(strict_types = 1);
namespace strsampler\Stream\Stdin;

use strsampler\Common\Script;
use strsampler\Stream\StreamFactoryInterface;
use strsampler\Stream\StreamInterface;

/**
 * Factory for stdin streams.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StdinStreamFactory implements StreamFactoryInterface
{
    /**
     * @var Script
     */
    private $script;

    /**
     * Creates a new factory.
     *
     * @param Script $script
     */
    public function __construct(Script $script)
    {
        $this->script = $script;
    }

    /**
     * Creates a new stdin stream.
     *
     * @param array $parameters
     *
     * @return StreamInterface
     */
    public function create(array $parameters): StreamInterface
    {
        return new StdinStream($this->script);
    }
}