<?php

declare(strict_types = 1);
namespace strsampler\Stream\Stdin;

use strsampler\Common\Script;
use strsampler\Stream\StreamInterface;

/**
 * Generates a stream of characters provided by the standard input (stdin).
 *
 * This stream ignores linebreaks.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StdinStream implements StreamInterface
{
    /**
     * @var Script
     */
    private $script;

    /**
     * Creates a new stdin stream.
     *
     * @param Script $script
     */
    public function __construct(Script $script)
    {
        $this->script = $script;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        $stdIn = $this->script->getStdin();

        while (!feof($stdIn)) {
            $character = fgetc($stdIn);
            if (false !== $character && "\n" !== $character) {
                yield $character;
            }
        }
    }
}