<?php

declare (strict_types = 1);

namespace strsampler\Application;

use Exception;
use strsampler\Benchmark\Benchmark;
use strsampler\Command\AbstractCommand;
use strsampler\Command\Argument\Argument;
use strsampler\Command\Argument\ArgumentParser;
use strsampler\Command\Option\Option;
use strsampler\Command\Option\OptionParser;
use strsampler\Common\Formatting;
use strsampler\Sampler\Sampler;
use strsampler\Stream\StreamFactory;

/**
 * The strsampler application.
 *
 * Run -h or --help for help.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StrSampler extends AbstractCommand
{
    const NAME = 'strsampler';
    const DESCRIPTION = 'samples stream characters';
    const VERSION = '1.0.0';

    /**
     * @var StreamFactory
     */
    private $streamFactory;
    /**
     * @var Sampler
     */
    private $sampler;
    /**
     * @var Benchmark
     */
    private $benchmark;

    /**
     * Constructs a new strsampler application.
     *
     * @param OptionParser   $optionParser
     * @param ArgumentParser $argumentParser
     * @param StreamFactory  $streamFactory
     * @param Sampler        $sampler
     * @param Benchmark      $benchmark
     */
    public function __construct(
        OptionParser $optionParser,
        ArgumentParser $argumentParser,
        StreamFactory $streamFactory,
        Sampler $sampler,
        Benchmark $benchmark
    ) {
        parent::__construct($optionParser, $argumentParser);
        $this->streamFactory = $streamFactory;
        $this->sampler = $sampler;
        $this->benchmark = $benchmark;

        $this->addOption(new Option('source', 'the data source (stdin|url|string|random)', 's'));
        $this->addOption(new Option('algorithm', 'the sampling algorithm (algorithmR)', 'a'));
        $this->addArgument(new Argument('sampleSize', 'the sample size', '5'));
        $this->addArgument(new Argument('input', 'the input (depending on source)'));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute()
    {
        try {
            list($source, $algorithm, $sampleSize, $input) = $this->getParameters();

            $stream = $this->streamFactory->create($source, [$input]);
            $sample = $this->sampler->sample($algorithm, $stream, $sampleSize);
            $memoryUsage = $this->benchmark->getPeakMemoryUsage();

            $this->printResult($sample, $memoryUsage);
        } catch (Exception $exception) {
            $this->printError($exception->getMessage());

            return;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDescription(): string
    {
        return self::DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    protected function getVersion(): string
    {
        return self::VERSION;
    }

    /**
     * Reads and processes the command line parameters.
     *
     * The parameters are returned as array with source, algorithm, sampleSize and input.
     *
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    private function getParameters()
    {
        $source = $this->getOption('source');
        $algorithm = $this->getOption('algorithm');
        $sampleSize = (int) $this->getArgument('sampleSize');
        $input = $this->getArgument('input');

        if (null === $algorithm) {
            $algorithm = 'algorithmR';
        }

        if (null === $source) {
            $source = 'stdin';
            $input = null;
        }

        if (null === $input && 'stdin' !== $source) {
            throw new \InvalidArgumentException('No input provided');
        }

        $source = (string) $source;
        $algorithm = (string) $algorithm;

        return [$source, $algorithm, $sampleSize, $input];
    }

    /**
     * Prints the result of the sampling operation.
     *
     * @param string $sample
     * @param int    $memoryUsage
     */
    private function printResult(string $sample, int $memoryUsage)
    {
        $formattedSample = $this->formatSample($sample);
        $formattedMemoryUsage = Formatting::formatBytes($memoryUsage);

        printf('Random Sample: %s'."\n", $formattedSample);
        printf("Maximum allocated memory during runtime: %s\n", $formattedMemoryUsage);
    }

    /**
     * Formats the sample for output, replacing all non printable characters with spaces.
     *
     * @param string $sample
     *
     * @return string
     */
    private function formatSample(string $sample): string
    {
        return preg_replace('#[\x00-\x1F\x80-\xFF]#', ' ', $sample);
    }

    /**
     * Prints error messages.
     *
     * @param string $message
     */
    private function printError(string $message)
    {
        printf("\033[31mError: %s\e[0m\n", $message);
    }
}
