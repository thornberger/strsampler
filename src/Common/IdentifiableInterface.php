<?php

declare(strict_types = 1);
namespace strsampler\Common;

/**
 * Provides access to the ID of an entity.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
interface IdentifiableInterface
{
    /**
     * @return string
     */
    public function getId(): string;
}