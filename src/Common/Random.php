<?php

namespace strsampler\Common;

/**
 * Provides access to random numbers.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Random
{
    /**
     * Returns a random integer between minimum and maximum (including both borders).
     *
     * This is a wrapper for the native random_int function.
     *
     * @param int $minimum
     * @param int $maximum
     *
     * @return int
     */
    public function getRandomInteger(int $minimum, int $maximum): int
    {
        return random_int($minimum, $maximum);
    }

    /**
     * Returns a random byte.
     *
     * This is a wrapper for the native random_bytes function with a fixed length of 1.
     *
     * @return string
     */
    public function getRandomByte(): string
    {
        return random_bytes(1);
    }
}