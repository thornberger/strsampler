<?php

declare(strict_types = 1);
namespace strsampler\Common;

/**
 * Provides access to the description of an entity.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
interface DescribableInterface
{
    /**
     * @return string
     */
    public function getDescription(): string;
}