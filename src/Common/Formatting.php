<?php

namespace strsampler\Common;

/**
 * Provides methods for formatting data.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Formatting
{
    /**
     * Formats a byte integer into a string such as 12 kB, 230 MB, etc.
     *
     * @param int $bytes
     *
     * @return string
     */
    public static function formatBytes(int $bytes): string
    {
        $base = log($bytes, 1024);
        $prefix = ['', 'k', 'M', 'G', 'T'];

        $floorBase = floor($base);
        $value = round(pow(1024, $base - $floorBase), 2);
        $unitPrefix = $prefix[(int) $floorBase];

        return sprintf('%d %sB', $value, $unitPrefix);
    }
}