<?php

declare(strict_types = 1);
namespace strsampler\Common;

/**
 * Provides access to script data such as arguments and options.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Script
{
    /**
     * Returns all arguments passed to the script.
     *
     * The first argument will always be the script name itself.
     * This method wraps access to the global $argv variable.
     *
     * @return array
     * @see $argv
     */
    public function getArguments() : array
    {
        global $argv;

        return $argv;
    }

    /**
     * Returns all options passed to the script, based on the supplied options.
     *
     * This method wraps the native getopt() function.
     *
     * @param string $options
     * @param array  $longOptions
     *
     * @return array
     * @see getopt()
     */
    public function getOptions(string $options, array $longOptions = []) : array
    {
        $options = getopt($options, $longOptions);

        if (false === $options) {
            return [];
        }

        return $options;
    }

    /**
     * Returns the STDIN resource.
     *
     * @return resource
     */
    public function getStdin()
    {
        return STDIN;
    }
}