<?php

declare(strict_types = 1);
namespace strsampler\Sampler\Exception;

use Exception;

/**
 * Thrown when trying to sample with an undefined sampling strategy.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UndefinedSamplingStrategyException extends Exception
{
    const MESSAGE_TEMPLATE = 'Undefined sampling strategy "%s"';

    /**
     * Creates a new exception for the given type.
     *
     * @param string $type
     */
    public function __construct($type)
    {
        parent::__construct(sprintf(self::MESSAGE_TEMPLATE, $type));
    }
}