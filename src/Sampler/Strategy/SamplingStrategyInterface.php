<?php

declare(strict_types = 1);
namespace strsampler\Sampler\Strategy;

use Traversable;

/**
 * Implements a sampling algorithm.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
interface SamplingStrategyInterface
{

    /**
     * Samples the data provided by a stream.
     *
     * @param Traversable $stream
     * @param int         $sampleSize
     *
     * @return array
     */
    public function sample(Traversable $stream, int $sampleSize): array;
}