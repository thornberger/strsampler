<?php

declare(strict_types = 1);
namespace strsampler\Sampler\Strategy;

use strsampler\Common\Random;
use Traversable;

/**
 * Implements the Algorithm R sampler by Jeffrey Vitter.
 *
 * http://www.cs.umd.edu/~samir/498/vitter.pdf
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class AlgorithmRSamplingStrategy implements SamplingStrategyInterface
{
    /**
     * @var Random
     */
    private $random;

    /**
     * Constructs a new algorithm R sampling strategy.
     *
     * @param Random $random
     */
    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    /**
     * {@inheritDoc}
     */
    public function sample(Traversable $stream, int $sampleSize): array
    {
        $index = 0;
        $sample = [];
        foreach ($stream as $value) {
            if ($index < $sampleSize) {
                $sample[$index] = $value;
            } else {
                $random = $this->random->getRandomInteger(0, $index);
                if ($random < $sampleSize) {
                    $sample[$random] = $value;
                }
            }

            $index++;
        }

        return $sample;
    }
}