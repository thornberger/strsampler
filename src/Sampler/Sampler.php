<?php

declare(strict_types = 1);
namespace strsampler\Sampler;

use strsampler\Sampler\Exception\UndefinedSamplingStrategyException;
use strsampler\Sampler\Strategy\SamplingStrategyInterface;

/**
 * Samples data provided by a traversable stream with different algorithms.
 *
 * This class is a strategy context that loads the required strategy based on the requested type.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Sampler
{
    /**
     * @var SamplingStrategyInterface[]
     */
    private $strategies = [];

    /**
     * Registers a sampling strategy.
     *
     * @param string                    $type
     * @param SamplingStrategyInterface $strategy
     */
    public function registerStrategy(string $type, SamplingStrategyInterface $strategy)
    {
        $this->strategies[$type] = $strategy;
    }

    /**
     * Samples stream data with the given type.
     *
     * @param string       $type
     * @param \Traversable $stream
     * @param int          $sampleSize
     *
     * @return string
     * @throws UndefinedSamplingStrategyException
     */
    public function sample(string $type, \Traversable $stream, int $sampleSize): string
    {
        if (!isset($this->strategies[$type])) {
            throw new UndefinedSamplingStrategyException($type);
        }

        $sampler = $this->strategies[$type];
        $sample = $sampler->sample($stream, $sampleSize);

        return $this->convertResult($sample);
    }

    /**
     * Converts the sampling result into a string.
     *
     * @param array $sample
     *
     * @return string
     */
    private function convertResult(array $sample): string
    {
        return implode('', $sample);
    }

}