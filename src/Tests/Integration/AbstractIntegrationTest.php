<?php

declare(strict_types = 1);
namespace strsampler\Tests\Integration;

use PHPUnit_Framework_TestCase;
use strsampler\Common\Formatting;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
abstract class AbstractIntegrationTest extends PHPUnit_Framework_TestCase
{
    /**
     * Returns the application path.
     *
     * @return string
     */
    abstract protected function getApplicationPath(): string;

    /**
     * Returns the memory limit in bytes.
     *
     * @return int
     */
    abstract protected function getMemoryLimit(): int;

    /**
     * Assert that the sample string has a specified size.
     *
     * @param string $sample
     * @param int    $sampleSize
     */
    protected function assertSampleSize(string $sample, int $sampleSize)
    {
        $actualSize = strlen($sample);

        $message = sprintf(
            'Failed asserting sample size. Expected size %d, but found size %d',
            $sampleSize,
            $actualSize
        );

        static::assertTrue($sampleSize == $actualSize, $message);
    }

    /**
     * Assert that the sample string contains only characters from the input.
     *
     * Spaces will be ignored since they are used for masking unprintable input.
     *
     * @param string $sample
     * @param string $input
     */
    protected function assertSampleCharacters(string $sample, string $input)
    {
        $unexpected = [];
        foreach (str_split($sample) as $character) {
            if (' ' !== $character && false === strpos($input, $character)) {
                $unexpected[] = $character;
            }
        }

        $message = '';
        if (!empty($unexpected)) {
            $unexpected = array_unique($unexpected);
            $unexpectedString = implode('","', $unexpected);
            $message = sprintf(
                'Unexpected characters: "%s" are not part of the input stream "%s"',
                $unexpectedString,
                $input
            );
        }

        static::assertEmpty($unexpected, $message);
    }

    /**
     * Assert that the memory consumption is lower than the given limit.
     *
     * @param int $bytes
     */
    protected function assertMemoryLimit(int $bytes)
    {
        $difference = $bytes - $this->getMemoryLimit();
        $message = sprintf('Failed asserting memory limit. Limit exceeded by %s', Formatting::formatBytes($difference));

        static::assertTrue(0 >= $difference, $message);
    }

    /**
     * Executes a command.
     *
     * Use the %s placeholder for the application path.
     *
     * @param string $command
     *
     * @return string
     */
    protected function executeCommand(string $command): string
    {
        $command = sprintf($command, $this->getApplicationPath());

        return shell_exec($command);
    }

    /**
     * Parses the command result.
     *
     * @param string $output
     *
     * @return array
     */
    protected function parseResult(string $output): array
    {
        $pattern = "#Random Sample: (?P<sample>.*)\nMaximum allocated memory during runtime: (?P<memory>\\d+) (?P<prefix>[k|M|G|T])*B#";
        $matches = [];

        preg_match($pattern, $output, $matches);

        $sample = $matches['sample'] ?? '';
        $memory = $matches['memory'];
        $prefix = $matches['prefix'] ?? '';

        $bytes = $this->convertToBytes($memory, $prefix);

        return [$sample, $bytes];
    }

    /**
     * Parses the error result.
     *
     * @param string $output
     *
     * @return string
     */
    protected function parseError(string $output): string
    {
        $length = strlen($output);

        return substr($output, 12, $length - 17);
    }

    /**
     * Converts byte numbers with unit prefix to bytes.
     *
     * @param string $number
     * @param string $unitPrefix
     *
     * @return int
     */
    private function convertToBytes(string $number, string $unitPrefix): int
    {
        $number = (int) $number;

        $prefixes = ['', 'k', 'M', 'G', 'T'];
        $index = array_search($unitPrefix, $prefixes);

        if (false === $index) {
            return $number;
        }

        return $number * pow(1024, $index);
    }
}