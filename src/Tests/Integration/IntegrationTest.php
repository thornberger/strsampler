<?php

declare(strict_types = 1);
namespace strsampler\Tests\Integration;

/**
 * Provides basic integration testing of strsampler.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class IntegrationTest extends AbstractIntegrationTest
{
    const APPLICATION_PATH = __DIR__.'/../../../bin/strsampler';
    const MEMORY_LIMIT = 1 * 1024 * 1024; // memory limit 1 MB

    /**
     * Test sampling from stdin.
     */
    public function testSampleFromStdin()
    {
        $input = $this->executeCommand('dd if=/dev/urandom count=100 bs=1 | base64');

        $echoCommand = sprintf('echo "%s"', $input);
        $result = $this->executeCommand($echoCommand.' | php %s 5');

        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertSampleCharacters($sample, $input);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from stdin with a short input.
     */
    public function testSampleFromStdinWithShortInput()
    {
        $input = $this->executeCommand('echo "hello"');

        $echoCommand = sprintf('echo "%s"', $input);
        $result = $this->executeCommand($echoCommand.' | php %s 10');

        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertSampleCharacters($sample, $input);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a string.
     */
    public function testSampleFromString()
    {
        $input = 'hello world';

        $result = $this->executeCommand('php %s --source=string 5 "hello world"');
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertSampleCharacters($sample, $input);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a short string.
     */
    public function testSampleFromShortString()
    {
        $input = 'hello';

        $result = $this->executeCommand('php %s --source=string 10 "hello"');
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertSampleCharacters($sample, $input);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a randomly generated stream of bytes.
     */
    public function testSampleFromRandom()
    {
        $result = $this->executeCommand('php %s --source=random 5 10');
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a short randomly generated stream of bytes.
     */
    public function testSampleFromShortRandom()
    {
        $result = $this->executeCommand('php %s --source=random 10 5');
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a URL.
     */
    public function testSampleFromUrl()
    {
        $result = $this->executeCommand(
            'php %s --source=url 5 "https://www.random.org/strings/?num=162&len=1&digits=on&upperalpha=on&loweralpha=on&format=plain"'
        );
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a URL.
     */
    public function testSampleFromFile()
    {
        $file = __DIR__.DIRECTORY_SEPARATOR.'mockfile';

        $result = $this->executeCommand(
            'php %s --source=url 5 '.'"'.$file.'"'
        );
        list($sample, $bytes) = $this->parseResult($result);

        $this->assertSampleSize($sample, 5);
        $this->assertMemoryLimit($bytes);
    }

    /**
     * Test sampling from a URL with an error returned.
     */
    public function testSampleFromUrlWithError()
    {
        $result = $this->executeCommand('php %s --source=url 5 "https://invalid"');
        $error = $this->parseError($result);

        $this->assertStringStartsWith('fopen', $error);
    }

    /**
     * Returns the aplication path.
     *
     * @return string
     */
    protected function getApplicationPath(): string
    {
        return self::APPLICATION_PATH;
    }

    /**
     * Returns the memory limit in bytes.
     *
     * @return int
     */
    protected function getMemoryLimit(): int
    {
        return self::MEMORY_LIMIT;
    }
}