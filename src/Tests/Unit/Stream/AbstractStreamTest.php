<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream;

use PHPUnit_Framework_TestCase;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
abstract class AbstractStreamTest extends PHPUnit_Framework_TestCase
{
    /**
     * @param array        $expected
     * @param \Traversable $iterator
     */
    public function assertIteratorResult(array $expected, \Traversable $iterator)
    {
        $result = [];
        foreach ($iterator as $key => $value) {
            $result[$key] = $value;
        }

        static::assertEquals($expected, $result);
    }
}