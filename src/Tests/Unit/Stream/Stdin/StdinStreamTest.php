<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Stdin;

use strsampler\Common\Script;
use strsampler\Stream\Stdin\StdinStream;
use strsampler\Tests\Unit\Stream\AbstractStreamTest;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StdinStreamTest extends AbstractStreamTest
{
    /**
     * @var Script|\PHPUnit_Framework_MockObject_MockObject
     */
    private $script;
    /**
     * @var StdinStream
     */
    private $stream;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->script = $this->getMockBuilder(Script::class)->getMock();

        $this->stream = new StdinStream($this->script);
    }

    public function testGetIterator()
    {
        $expected = ['h', 'e', 'l', 'l', 'o'];
        $input = 'hello';

        $resource = $this->getMockResource($input);
        $this->script->expects(static::once())->method('getStdin')->willReturn($resource);

        $iterator = $this->stream->getIterator();

        $this->assertIteratorResult($expected, $iterator);
    }

    public function testGetIteratorWithLineBreak()
    {
        $expected = ['h', 'e', 'l', 'l', 'o'];
        $input = "hel\nlo";

        $resource = $this->getMockResource($input);
        $this->script->expects(static::once())->method('getStdin')->willReturn($resource);

        $iterator = $this->stream->getIterator();

        $this->assertIteratorResult($expected, $iterator);
    }

    public function testGetIteratorWithNullByte()
    {
        $expected = ['h', 'e', 'l', chr(0), 'l', 'o'];
        $input = "hel\0lo";

        $resource = $this->getMockResource($input);
        $this->script->expects(static::once())->method('getStdin')->willReturn($resource);

        $iterator = $this->stream->getIterator();

        $this->assertIteratorResult($expected, $iterator);
    }

    /**
     * @param string $input
     *
     * @return resource
     */
    private function getMockResource(string $input)
    {
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, $input);
        rewind($stream);

        return $stream;
    }
}
