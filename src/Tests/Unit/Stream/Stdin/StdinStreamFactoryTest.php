<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Stdin;

use strsampler\Common\Script;
use strsampler\Stream\Stdin\StdinStream;
use strsampler\Stream\Stdin\StdinStreamFactory;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StdinStreamFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Script|\PHPUnit_Framework_MockObject_MockObject
     */
    private $script;
    /**
     * @var StdinStreamFactory
     */
    private $factory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->script = $this->getMockBuilder(Script::class)->getMock();

        $this->factory = new StdinStreamFactory($this->script);
    }

    public function testCreate()
    {
        $expected = new StdinStream($this->script);

        $result = $this->factory->create([]);
        static::assertEquals($expected, $result);
    }
}
