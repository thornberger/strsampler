<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Exception;

use PHPUnit_Framework_TestCase;
use strsampler\Stream\Exception\UndefinedStreamTypeException;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UndefinedStreamTypeExceptionTest extends PHPUnit_Framework_TestCase
{
    const TYPE = 'undefined';

    /**
     * @var UndefinedStreamTypeException
     */
    private $exception;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->exception = new UndefinedStreamTypeException(self::TYPE);
    }

    public function testGetMessage()
    {
        $expected = 'Undefined stream type "undefined"';
        $result = $this->exception->getMessage();

        static::assertSame($expected, $result);
    }
}
