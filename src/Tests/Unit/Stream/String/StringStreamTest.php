<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\String;

use strsampler\Stream\String\StringStream;
use strsampler\Tests\Unit\Stream\AbstractStreamTest;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StringStreamTest extends AbstractStreamTest
{
    const STRING = 'hello';

    /**
     * @var StringStream
     */
    private $stream;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->stream = new StringStream(self::STRING);
    }

    public function testGetIterator()
    {
        $expected = ['h', 'e', 'l', 'l', 'o'];
        $iterator = $this->stream->getIterator();

        $result = [];
        foreach ($iterator as $key => $value) {
            $result[$key] = $value;
        }

        $this->assertIteratorResult($expected, $iterator);
    }
}
