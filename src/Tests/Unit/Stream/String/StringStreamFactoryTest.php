<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\String;

use strsampler\Stream\String\StringStream;
use strsampler\Stream\String\StringStreamFactory;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StringStreamFactoryTest extends \PHPUnit_Framework_TestCase
{
    const STRING = 'my happy string';

    /**
     * @var StringStreamFactory
     */
    private $factory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->factory = new StringStreamFactory();
    }

    public function testCreate()
    {
        $expected = new StringStream(self::STRING);

        $result = $this->factory->create([self::STRING]);
        static::assertEquals($expected, $result);
    }
}
