<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Random;

use strsampler\Common\Random;
use strsampler\Stream\Random\RandomStream;
use strsampler\Tests\Unit\Stream\AbstractStreamTest;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class RandomStreamTest extends AbstractStreamTest
{
    const LENGTH = 5;

    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $random;
    /**
     * @var RandomStream
     */
    private $stream;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->random = $this->getMockBuilder(Random::class)->disableOriginalConstructor()->getMock();
        $this->stream = new RandomStream($this->random, self::LENGTH);
    }

    public function testGetIterator()
    {
        $expected = ['h', 'e', 'l', 'l', 'o'];

        $this->random->expects(static::at(0))->method('getRandomByte')->willReturn('h');
        $this->random->expects(static::at(1))->method('getRandomByte')->willReturn('e');
        $this->random->expects(static::at(2))->method('getRandomByte')->willReturn('l');
        $this->random->expects(static::at(3))->method('getRandomByte')->willReturn('l');
        $this->random->expects(static::at(4))->method('getRandomByte')->willReturn('o');

        $iterator = $this->stream->getIterator();

        $this->assertIteratorResult($expected, $iterator);
    }
}
