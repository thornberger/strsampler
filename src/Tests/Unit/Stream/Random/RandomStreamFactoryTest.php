<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Random;

use strsampler\Common\Random;
use strsampler\Stream\Random\RandomStream;
use strsampler\Stream\Random\RandomStreamFactory;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class RandomStreamFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $random;
    /**
     * @var RandomStreamFactory
     */
    private $factory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->random = $this->getMockBuilder(Random::class)->getMock();

        $this->factory = new RandomStreamFactory($this->random);
    }

    public function testCreate()
    {
        $expected = new RandomStream($this->random, 3);

        $result = $this->factory->create(['3']);
        static::assertEquals($expected, $result);
    }
}
