<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream;

use strsampler\Stream\StreamFactory;
use strsampler\Stream\StreamFactoryInterface;
use strsampler\Stream\StreamInterface;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StreamFactoryTest extends \PHPUnit_Framework_TestCase
{
    const FACTORY_1 = 'factory1';
    const FACTORY_2 = 'factory2';

    /**
     * @var StreamFactoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $factory1;
    /**
     * @var StreamFactoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $factory2;
    /**
     * @var StreamInterface
     */
    private $stream;
    /**
     * @var StreamFactory
     */
    private $streamFactory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->factory1 = $this->getMockBuilder(StreamFactoryInterface::class)->getMock();
        $this->factory2 = $this->getMockBuilder(StreamFactoryInterface::class)->getMock();
        $this->stream = $this->getMockBuilder(StreamInterface::class)->getMock();

        $this->streamFactory = new StreamFactory();

        $this->streamFactory->registerFactory(self::FACTORY_1, $this->factory1);
        $this->streamFactory->registerFactory(self::FACTORY_2, $this->factory2);
    }

    /**
     * @expectedException \strsampler\Stream\Exception\UndefinedStreamTypeException
     */
    public function testCreateWithUndefinedStreamType()
    {
        $this->streamFactory->create('undefined', []);
    }

    public function testCreate()
    {
        $parameters = ['some'];
        $this->factory1->expects(static::once())->method('create')->with($parameters)->willReturn($this->stream);

        $result = $this->streamFactory->create(self::FACTORY_1, $parameters);

        static::assertSame($this->stream, $result);
    }
}
