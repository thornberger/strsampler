<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Stdin;

use strsampler\Stream\Url\UrlStream;
use strsampler\Tests\Unit\Stream\AbstractStreamTest;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStreamTest extends AbstractStreamTest
{
    const MOCKFILE = 'mockfile';

    /**
     * @var UrlStream
     */
    private $stream;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->stream = new UrlStream($this->getMockUrl());
    }

    public function testGetIterator()
    {
        $expected = ['h', 'e', 'l', 'l', 'o'];

        $iterator = $this->stream->getIterator();

        $this->assertIteratorResult($expected, $iterator);
    }

    private function getMockUrl()
    {
        return __DIR__.DIRECTORY_SEPARATOR.self::MOCKFILE;
    }
}
