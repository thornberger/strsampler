<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Url;

use PHPUnit_Framework_TestCase;
use strsampler\Stream\Url\UrlStreamException;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStreamExceptionTest extends PHPUnit_Framework_TestCase
{
    const URL = 'http://www.someurl.net';
    const MESSAGE = 'some message';
    /**
     * @var UrlStreamException
     */
    private $exception;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->exception = new UrlStreamException(self::URL, self::MESSAGE);
    }

    public function testGetMessage()
    {
        $result = $this->exception->getMessage();
        static::assertSame(self::MESSAGE, $result);
    }

    public function testGetUrl()
    {
        $result = $this->exception->getUrl();
        static::assertSame(self::URL, $result);
    }
}
