<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Stream\Url;

use strsampler\Stream\Url\UrlStream;
use strsampler\Stream\Url\UrlStreamFactory;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UrlStreamFactoryTest extends \PHPUnit_Framework_TestCase
{
    const URL = 'http://www.someurl.com';

    /**
     * @var UrlStreamFactory
     */
    private $factory;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->factory = new UrlStreamFactory();
    }

    public function testCreate()
    {
        $expected = new UrlStream(self::URL);

        $result = $this->factory->create([self::URL]);
        static::assertEquals($expected, $result);
    }
}
