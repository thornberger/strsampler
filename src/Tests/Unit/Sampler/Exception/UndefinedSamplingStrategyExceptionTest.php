<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Sampler\Exception;

use strsampler\Sampler\Exception\UndefinedSamplingStrategyException;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class UndefinedSamplingStrategyExceptionTest extends \PHPUnit_Framework_TestCase
{
    const TYPE = 'undefined';

    /**
     * @var UndefinedSamplingStrategyException
     */
    private $exception;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->exception = new UndefinedSamplingStrategyException(self::TYPE);
    }

    public function testGetMessage()
    {
        $expected = 'Undefined sampling strategy "undefined"';

        $result = $this->exception->getMessage();
        static::assertSame($expected, $result);
    }
}
