<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Sampler;

use strsampler\Sampler\Sampler;
use strsampler\Sampler\Strategy\SamplingStrategyInterface;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class SamplerTest extends \PHPUnit_Framework_TestCase
{
    const STRATEGY_1 = 'strategy1';
    const STRATEGY_2 = 'strategy2';
    const SAMPLE_SIZE = 5;

    /**
     * @var SamplingStrategyInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $strategy1;
    /**
     * @var SamplingStrategyInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $strategy2;
    /**
     * @var \Traversable|\PHPUnit_Framework_MockObject_MockObject
     */
    private $stream;
    /**
     * @var Sampler
     */
    private $sampler;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->strategy1 = $this->getMockBuilder(SamplingStrategyInterface::class)->getMock();
        $this->strategy2 = $this->getMockBuilder(SamplingStrategyInterface::class)->getMock();
        $this->stream = $this->getMockBuilder(\Traversable::class)->getMock();

        $this->sampler = new Sampler();

        $this->sampler->registerStrategy(self::STRATEGY_1, $this->strategy1);
    }

    /**
     * @expectedException \strsampler\Sampler\Exception\UndefinedSamplingStrategyException
     */
    public function testSampleWithUndefinedStrategy()
    {
        $this->sampler->sample(self::STRATEGY_2, $this->stream, self::SAMPLE_SIZE);
    }

    public function testSample()
    {
        $expected = 'hello';
        $sampled = ['h', 'e', 'l', 'l', 'o'];

        $this->strategy1->expects(static::once())
            ->method('sample')
            ->with($this->stream, self::SAMPLE_SIZE)
            ->willReturn($sampled);
        $result = $this->sampler->sample(self::STRATEGY_1, $this->stream, self::SAMPLE_SIZE);

        static::assertEquals($expected, $result);
    }
}
