<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Sampler\Strategy;

use strsampler\Common\Random;
use strsampler\Sampler\Strategy\AlgorithmRSamplingStrategy;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class AlgorithmRSamplingStrategyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $random;
    /**
     * @var AlgorithmRSamplingStrategy
     */
    private $strategy;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->random = $this->getMockBuilder(Random::class)->disableOriginalConstructor()->getMock();

        $this->strategy = new AlgorithmRSamplingStrategy($this->random);
    }

    /**
     * @dataProvider indexSmallerSampleSize
     *
     * @param array              $expected
     * @param array|\Traversable $stream
     * @param int                $sampleSize
     */
    public function testSampleWithLengthUpToSampleSize(array $expected, array $stream, int $sampleSize)
    {
        $result = $this->strategy->sample(new \ArrayIterator($stream), $sampleSize);
        $this->random->expects(static::never())->method('getRandomInteger');
        static::assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function indexSmallerSampleSize()
    {
        return [
            [[], [], 5],
            [['h'], ['h'], 5],
            [['h', 'e'], ['h', 'e'], 5],
        ];
    }

    public function testSampleWithLengthLargerThanSampleSize()
    {
        $expected = ['l', 'o', 'l'];
        $input = 'helloworld';
        $sampleSize = 3;

        // initial: 'hel'
        $this->prepareRandom(0, 3, 2); // l, replace: hel => hll
        $this->prepareRandom(1, 4, 4); // o, no replacement: hll
        $this->prepareRandom(2, 5, 1); // w, replace: hll => hwl
        $this->prepareRandom(3, 6, 1); // o, replace: hwl => hol
        $this->prepareRandom(4, 7, 5); // r, no replacement: hol
        $this->prepareRandom(5, 8, 0); // l, replace: hol => lol
        $this->prepareRandom(6, 9, 8); // d, no replacement: lol

        $result = $this->strategy->sample(new \ArrayIterator(str_split($input)), $sampleSize);
        static::assertSame($expected, $result);
    }

    /**
     * @param int $callIndex
     * @param int $iterationIndex
     * @param int $result
     */
    private function prepareRandom(int $callIndex, int $iterationIndex, int $result)
    {
        $this->random->expects(static::at($callIndex))
            ->method('getRandomInteger')
            ->with(0, $iterationIndex)
            ->willReturn($result);
    }
}
