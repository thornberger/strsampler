<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Benchmark;

use PHPUnit_Framework_TestCase;
use strsampler\Benchmark\Benchmark;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class BenchmarkTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Benchmark
     */
    private $benchmark;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->benchmark = new Benchmark();
    }

    public function testGetPeakMemoryUsage()
    {
        $result = $this->benchmark->getPeakMemoryUsage();

        $this->assertInternalType('integer', $result);
        $this->assertTrue(0 < $result);
    }
}
