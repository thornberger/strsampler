<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Common\Formatting;

use PHPUnit_Framework_TestCase;
use strsampler\Common\Formatting;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class FormattingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider formatBytesProvider
     *
     * @param string $expected
     * @param int    $bytes
     */
    public function testFormatBytes(string $expected, int $bytes)
    {
        $result = Formatting::formatBytes($bytes);

        static::assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function formatBytesProvider()
    {
        return [
            ['12 B', 12],
            ['1 kB', 1024],
            ['1 MB', 1024 * 1024],
            ['1 GB', 1024 * 1024 * 1024],
            ['1 TB', 1024 * 1024 * 1024 * 1024],
            // rounding
            ['1 kB', 1028],
            ['3 MB', 3 * 1024 * 1024 + 1024],
        ];
    }
}