<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Common;

use PHPUnit_Framework_TestCase;
use strsampler\Common\Script;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class ScriptTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Script
     */
    private $script;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->script = new Script();
    }

    public function testGetArguments()
    {
        $result = $this->script->getArguments();

        static::assertInternalType('array', $result);
        static::assertTrue(1 <= count($result));
    }

    public function testGetOptions()
    {
        $result = $this->script->getOptions('');
        static::assertInternalType('array', $result);
    }

    public function testGetStdin()
    {
        $result = $this->script->getStdin();
        static::assertInternalType('resource', $result);
    }
}