<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Common\Formatting;

use strsampler\Common\Random;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class RandomTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Random
     */
    private $random;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->random = new Random();
    }

    public function testGetRandomInteger()
    {
        $minimum = 2;
        $maximum = 10;

        $result = $this->random->getRandomInteger($minimum, $maximum);

        static::assertInternalType('int', $result);
        static::assertTrue($minimum <= $result);
        static::assertTrue($maximum >= $result);
    }

    public function testGetRandomByte()
    {
        $result = $this->random->getRandomByte();

        static::assertInternalType('string', $result);
        static::assertTrue(1 === strlen($result));
    }
}
