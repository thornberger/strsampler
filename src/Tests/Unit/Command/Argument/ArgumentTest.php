<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Command\Argument;

use strsampler\Command\Argument\Argument;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class ArgumentTest extends \PHPUnit_Framework_TestCase
{
    const ID = 'some_id';
    const DESCRIPTION = 'some description';
    const DEFAULT_VALUE = '12';

    /**
     * @var Argument
     */
    private $argument;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->argument = new Argument(self::ID, self::DESCRIPTION, self::DEFAULT_VALUE);
    }

    public function testDefaultConstructorArguments()
    {
        $argument = new Argument(self::ID);

        static::assertSame('', $argument->getDescription());
        static::assertNull($argument->getDefaultValue());
    }

    public function testGetId()
    {
        $result = $this->argument->getId();
        static::assertSame(self::ID, $result);
    }

    public function testGetDescription()
    {
        $result = $this->argument->getDescription();
        static::assertSame(self::DESCRIPTION, $result);
    }

    public function testGetDefaultValue()
    {
        $result = $this->argument->getDefaultValue();
        static::assertSame(self::DEFAULT_VALUE, $result);
    }
}
