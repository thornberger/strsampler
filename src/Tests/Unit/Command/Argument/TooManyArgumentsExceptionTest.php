<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Command\Argument;

use strsampler\Command\Argument\TooManyArgumentsException;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class TooManyArgumentsExceptionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    private static $superfluousArguments = ['hello', 'world'];

    /**
     * @var TooManyArgumentsException
     */
    private $exception;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->exception = new TooManyArgumentsException(self::$superfluousArguments);
    }

    public function testGetSuperfluousArguments()
    {
        static::assertEquals(self::$superfluousArguments, $this->exception->getSuperfluousArguments());
    }

    public function testGetMessage()
    {
        $expected = 'Superfluous arguments [hello, world]';

        static::assertSame($expected, $this->exception->getMessage());
    }
}
