<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Command\Argument;

use strsampler\Command\Argument\Argument;
use strsampler\Command\Argument\ArgumentParser;
use strsampler\Common\Script;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class ArgumentParserTest extends \PHPUnit_Framework_TestCase
{
    const SCRIPT_NAME = 'myscript.php';
    const ID = 'some_id';

    /**
     * @var Script|\PHPUnit_Framework_MockObject_MockObject
     */
    private $script;
    /**
     * @var ArgumentParser
     */
    private $parser;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->script = $this->getMockBuilder(Script::class)->getMock();

        $this->parser = new ArgumentParser($this->script);
    }

    public function testGettingAllDefinedArguments()
    {
        $argument1 = new Argument('1');
        $argument2 = new Argument('2');
        $expected = [$argument1, $argument2];

        $this->parser->add($argument1);
        $this->parser->add($argument2);

        $result = $this->parser->getAllDefinedArguments();

        static::assertEquals($expected, $result);
    }

    public function testGetWithDefinedArgument()
    {
        $expected = 'value';
        $arguments = ['value'];

        $this->prepareScript($arguments);
        $this->parser->add(new Argument(self::ID));
        $this->parser->parse();

        $result = $this->parser->get(self::ID);
        static::assertSame($expected, $result);
    }

    public function testGetWithMissingArgument()
    {
        $expected = null;
        $arguments = [];

        $this->prepareScript($arguments);
        $this->parser->add(new Argument(self::ID));
        $this->parser->parse();

        $result = $this->parser->get(self::ID);
        static::assertSame($expected, $result);
    }

    public function testGetWithDefaultArgument()
    {
        $default = 'value';
        $arguments = [];

        $this->prepareScript($arguments);
        $this->parser->add(new Argument(self::ID, '', $default));
        $this->parser->parse();

        $result = $this->parser->get(self::ID);
        static::assertSame($default, $result);
    }

    /**
     * @expectedException \strsampler\Command\Argument\TooManyArgumentsException
     */
    public function testGetWithSuperfluousArguments()
    {
        $arguments = ['some', 'thing'];

        $this->prepareScript($arguments);
        $this->parser->add(new Argument(self::ID));
        $this->parser->parse();
    }

    public function testGetWithSkip()
    {
        $expected = 'value';
        $arguments = ['some', 'value'];

        $this->prepareScript($arguments);
        $this->parser->add(new Argument(self::ID));
        $this->parser->parse(1);

        $result = $this->parser->get(self::ID);
        static::assertSame($expected, $result);
    }

    /**
     * @param array $arguments
     */
    private function prepareScript(array $arguments)
    {
        $arguments = array_merge([self::SCRIPT_NAME], $arguments);
        $this->script->expects(static::once())->method('getArguments')->willReturn($arguments);
    }
}
