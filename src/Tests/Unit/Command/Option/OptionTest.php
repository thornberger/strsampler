<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Command\Argument;

use strsampler\Command\Option\Option;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class OptionTest extends \PHPUnit_Framework_TestCase
{
    const ID = 'some_id';
    const DESCRIPTION = 'some description';
    const SHORT_CUT = 's';

    /**
     * @var Option
     */
    private $option;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->option = new Option(self::ID, self::DESCRIPTION, self::SHORT_CUT);
    }

    public function testDefaultConstructorArguments()
    {
        $option = new Option(self::ID);

        static::assertSame('', $option->getDescription());
        static::assertSame('', $option->getShortCut());
    }

    public function testGetId()
    {
        $result = $this->option->getId();
        static::assertSame(self::ID, $result);
    }

    public function testGetDescription()
    {
        $result = $this->option->getDescription();
        static::assertSame(self::DESCRIPTION, $result);
    }

    public function testGetShortCut()
    {
        $result = $this->option->getShortCut();
        static::assertSame(self::SHORT_CUT, $result);
    }
}
