<?php
declare(strict_types = 1);
namespace strsampler\Tests\Unit\Command\Option;

use PHPUnit_Framework_TestCase;
use strsampler\Command\Option\Flag;
use strsampler\Command\Option\Option;
use strsampler\Command\Option\OptionParser;
use strsampler\Common\Script;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class OptionParserTest extends PHPUnit_Framework_TestCase
{
    const ID = 'some_id';
    const SHORT_CUT = 's';

    /**
     * @var Script|\PHPUnit_Framework_MockObject_MockObject
     */
    private $script;
    /**
     * @var OptionParser
     */
    private $parser;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->script = $this->getMockBuilder(Script::class)->getMock();

        $this->parser = new OptionParser($this->script);
    }

    public function testGettingAllDefinedOptions()
    {
        $option1 = new Option('1');
        $option2 = new Option('2');
        $expected = [$option1, $option2];

        $this->parser->add($option1);
        $this->parser->add($option2);

        $result = $this->parser->getAllDefinedOptions();

        static::assertEquals($expected, $result);
    }

    public function testGetWithShortOption()
    {
        $expected = 'value';
        $options = [self::SHORT_CUT => 'value'];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT.':', [self::ID.':'])
            ->willReturn($options);

        $this->parser->add(new Option(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertSame($expected, $result);
    }

    public function testGetWithLongOption()
    {
        $expected = 'value';
        $options = [
            self::ID => 'value',
        ];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with('', [self::ID.':'])
            ->willReturn($options);

        $this->parser->add(new Option(self::ID, ''));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertSame($expected, $result);
    }

    public function testGetWithLongOptionOverridingShortOption()
    {
        $expected = 'value';
        $options = [
            self::SHORT_CUT => 'short_value',
            self::ID        => 'value',
        ];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT.':', [self::ID.':'])
            ->willReturn($options);

        $this->parser->add(new Option(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertSame($expected, $result);
    }

    public function testGetWithFlag()
    {
        $options = [self::SHORT_CUT => false];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT, [self::ID])
            ->willReturn($options);

        $this->parser->add(new Flag(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertTrue($result);
    }

    public function testGetWithMissingOption()
    {
        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT.':', [self::ID.':'])
            ->willReturn([]);

        $this->parser->add(new Option(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertNull($result);
    }

    public function testGetWithMissingFlag()
    {
        $options = [];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT, [self::ID])
            ->willReturn($options);

        $this->parser->add(new Flag(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->get(self::ID);

        static::assertFalse($result);
    }

    public function testCounting()
    {
        $expected = 2;
        $options = [
            self::SHORT_CUT => false,
            'something'     => 'else',
        ];

        $this->script->expects(static::once())
            ->method('getOptions')
            ->with(self::SHORT_CUT, [self::ID])
            ->willReturn($options);

        $this->parser->add(new Flag(self::ID, '', self::SHORT_CUT));
        $this->parser->parse();
        $result = $this->parser->getNumberOfReadOptions();

        static::assertSame($expected, $result);
    }
}
