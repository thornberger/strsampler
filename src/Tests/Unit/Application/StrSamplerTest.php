<?php

declare(strict_types = 1);
namespace strsampler\Tests\Unit\Application;

use PHPUnit_Framework_TestCase;
use strsampler\Application\StrSampler;
use strsampler\Benchmark\Benchmark;
use strsampler\Command\Argument\ArgumentParser;
use strsampler\Command\Option\OptionParser;
use strsampler\Sampler\Sampler;
use strsampler\Stream\StreamFactory;
use strsampler\Stream\StreamInterface;

/**
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class StrSamplerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var OptionParser|\PHPUnit_Framework_MockObject_MockObject
     */
    private $optionParser;
    /**
     * @var ArgumentParser|\PHPUnit_Framework_MockObject_MockObject
     */
    private $argumentParser;
    /**
     * @var Benchmark|\PHPUnit_Framework_MockObject_MockObject
     */
    private $benchmark;
    /**
     * @var Sampler|\PHPUnit_Framework_MockObject_MockObject
     */
    private $sampler;
    /**
     * @var StreamFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $streamFactory;
    /**
     * @var StreamInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $stream;
    /**
     * @var StrSampler
     */
    private $strSampler;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->optionParser = $this->getMockBuilder(OptionParser::class)->disableOriginalConstructor()->getMock();
        $this->argumentParser = $this->getMockBuilder(ArgumentParser::class)->disableOriginalConstructor()->getMock();
        $this->benchmark = $this->getMockBuilder(Benchmark::class)->disableOriginalConstructor()->getMock();
        $this->sampler = $this->getMockBuilder(Sampler::class)->disableOriginalConstructor()->getMock();
        $this->streamFactory = $this->getMockBuilder(StreamFactory::class)->disableOriginalConstructor()->getMock();
        $this->stream = $this->getMockBuilder(StreamInterface::class)->disableOriginalConstructor()->getMock();

        $this->strSampler = new StrSampler(
            $this->optionParser,
            $this->argumentParser,
            $this->streamFactory,
            $this->sampler,
            $this->benchmark
        );
    }

    public function testRunWithVersion()
    {
        $expected = StrSampler::NAME.' '.StrSampler::VERSION."\n";
        $skip = 2;

        $this->optionParser->expects(static::once())->method('parse');
        $this->optionParser->expects(static::once())->method('getNumberOfReadOptions')->willReturn($skip);
        $this->argumentParser->expects(static::once())->method('parse')->with($skip);

        $this->optionParser->expects(static::once())->method('get')->with('version')->willReturn(true);

        static::expectOutputString($expected);

        $this->strSampler->run();
    }

    public function testRunWithHelp()
    {
        $expected = '#'.preg_quote(StrSampler::NAME.' '.StrSampler::VERSION.' -- '.StrSampler::DESCRIPTION."\n").'#';
        $skip = 2;

        $this->optionParser->expects(static::at(0))->method('parse');
        $this->optionParser->expects(static::at(1))->method('getNumberOfReadOptions')->willReturn($skip);
        $this->argumentParser->expects(static::once())->method('parse')->with($skip);

        $this->optionParser->expects(static::at(2))->method('get')->with('version')->willReturn(false);
        $this->optionParser->expects(static::at(3))->method('get')->with('help')->willReturn(true);

        static::expectOutputRegex($expected);

        $this->strSampler->run();
    }

    public function testRunWithMissingInput()
    {
        $expected = '#No input provided#';
        $source = 'string';
        $sampleSize = 5;

        $this->prepareParsersForExecute();

        $this->optionParser->expects(static::at(4))->method('get')->with('source')->willReturn($source);
        $this->optionParser->expects(static::at(5))->method('get')->with('algorithm')->willReturn(null);
        $this->argumentParser->expects(static::at(1))->method('get')->with('sampleSize')->willReturn($sampleSize);
        $this->argumentParser->expects(static::at(2))->method('get')->with('input')->willReturn(null);

        $this->streamFactory->expects(static::never())->method('create');
        $this->sampler->expects(static::never())->method('sample');

        static::expectOutputRegex($expected);

        $this->strSampler->run();
    }

    public function testRunWithException()
    {
        $expected = '#Error: some message#';
        $message = 'some message';
        $source = 'string';
        $input = 'HELLOWORLD';
        $sampleSize = 5;

        $this->prepareParsersForExecute();

        $this->optionParser->expects(static::at(4))->method('get')->with('source')->willReturn($source);
        $this->optionParser->expects(static::at(5))->method('get')->with('algorithm')->willReturn(null);
        $this->argumentParser->expects(static::at(1))->method('get')->with('sampleSize')->willReturn($sampleSize);
        $this->argumentParser->expects(static::at(2))->method('get')->with('input')->willReturn($input);

        $this->streamFactory
            ->expects(static::once())
            ->method('create')
            ->with($source, [$input])
            ->willThrowException(new \Exception($message));
        $this->sampler->expects(static::never())->method('sample');

        static::expectOutputRegex($expected);

        $this->strSampler->run();
    }

    public function testRunWithDefaultSourceAndAlgorithm()
    {
        $sample = 'WORLD';
        $expected = '#^Random Sample: '.$sample.'#';
        $sampleSize = 5;
        $expectedSource = 'stdin';
        $expectedInput = null;
        $expectedAlgorithm = 'algorithmR';

        $this->prepareParsersForExecute();

        $this->optionParser->expects(static::at(4))->method('get')->with('source')->willReturn(null);
        $this->optionParser->expects(static::at(5))->method('get')->with('algorithm')->willReturn(null);
        $this->argumentParser->expects(static::at(1))->method('get')->with('sampleSize')->willReturn($sampleSize);
        $this->argumentParser->expects(static::at(2))->method('get')->with('input')->willReturn(null);

        $this->streamFactory
            ->expects(static::once())
            ->method('create')
            ->with($expectedSource, [$expectedInput])
            ->willReturn($this->stream);
        $this->sampler
            ->expects(static::once())
            ->method('sample')
            ->with($expectedAlgorithm, $this->stream, $sampleSize)
            ->willReturn($sample);

        static::expectOutputRegex($expected);

        $this->strSampler->run();
    }

    public function testRunWithDefinedSourceAndAlgorithm()
    {
        $sample = 'WORLD';
        $expected = '#^Random Sample: '.$sample.'#';
        $sampleSize = 5;
        $input = 'HELLOWORLD';
        $source = 'string';
        $algorithm = 'algorithmR';

        $this->prepareParsersForExecute();

        $this->optionParser->expects(static::at(4))->method('get')->with('source')->willReturn($source);
        $this->optionParser->expects(static::at(5))->method('get')->with('algorithm')->willReturn($algorithm);
        $this->argumentParser->expects(static::at(1))->method('get')->with('sampleSize')->willReturn($sampleSize);
        $this->argumentParser->expects(static::at(2))->method('get')->with('input')->willReturn($input);

        $this->streamFactory
            ->expects(static::once())
            ->method('create')
            ->with($source, [$input])
            ->willReturn($this->stream);
        $this->sampler
            ->expects(static::once())
            ->method('sample')
            ->with($algorithm, $this->stream, $sampleSize)
            ->willReturn($sample);

        static::expectOutputRegex($expected);

        $this->strSampler->run();
    }

    /**
     * Prepares the parser mocks for running the execute method.
     */
    private function prepareParsersForExecute()
    {
        $skip = 2;
        $this->optionParser->expects(static::at(0))->method('parse');
        $this->optionParser->expects(static::at(1))->method('getNumberOfReadOptions')->willReturn($skip);
        $this->argumentParser->expects(static::at(0))->method('parse')->with($skip);

        $this->optionParser->expects(static::at(2))->method('get')->with('version')->willReturn(false);
        $this->optionParser->expects(static::at(3))->method('get')->with('help')->willReturn(false);
    }
}
