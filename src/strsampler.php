#!/usr/bin/env php
<?php

require_once __DIR__.'/../vendor/autoload.php';

$script = new \strsampler\Common\Script();
$random = new \strsampler\Common\Random();
$optionParser = new \strsampler\Command\Option\OptionParser($script);
$argumentParser = new \strsampler\Command\Argument\ArgumentParser($script);
$streamFactory = new \strsampler\Stream\StreamFactory();
$streamFactory->registerFactory('stdin', new \strsampler\Stream\Stdin\StdinStreamFactory($script));
$streamFactory->registerFactory('string', new \strsampler\Stream\String\StringStreamFactory());
$streamFactory->registerFactory('url', new \strsampler\Stream\Url\UrlStreamFactory());
$streamFactory->registerFactory('random', new \strsampler\Stream\Random\RandomStreamFactory($random));
$sampler = new \strsampler\Sampler\Sampler();
$sampler->registerStrategy('algorithmR', new \strsampler\Sampler\Strategy\AlgorithmRSamplingStrategy($random));
$benchmark = new \strsampler\Benchmark\Benchmark();

$application = new \strsampler\Application\StrSampler(
    $optionParser,
    $argumentParser,
    $streamFactory,
    $sampler,
    $benchmark
);
$application->run();