<?php

declare(strict_types = 1);
namespace strsampler\Command\Argument;

use Exception;

/**
 * Thrown when the argument parser found more than the defined number of arguments.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class TooManyArgumentsException extends Exception
{
    const MESSAGE_TEMPLATE = 'Superfluous arguments [%s]';

    /**
     * @var array
     */
    private $superfluousArguments;

    /**
     * Creates a new exception for the given array of superfluous arguments.
     *
     * @param string[] $superfluousArguments
     */
    public function __construct(array $superfluousArguments)
    {
        $message = $this->createMessage($superfluousArguments);
        parent::__construct($message);

        $this->superfluousArguments = $superfluousArguments;
    }

    /**
     * Returns the superfluous arguments.
     *
     * @return array
     */
    public function getSuperfluousArguments(): array
    {
        return $this->superfluousArguments;
    }

    /**
     * Creates the exception message.
     *
     * @param array $arguments
     *
     * @return string
     */
    private function createMessage(array $arguments): string
    {
        $stringified = implode(', ', $arguments);

        return sprintf(self::MESSAGE_TEMPLATE, $stringified);
    }
}