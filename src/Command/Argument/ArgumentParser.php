<?php
declare(strict_types = 1);
namespace strsampler\Command\Argument;

use strsampler\Common\Script;

/**
 * Parses command line arguments.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class ArgumentParser
{
    /**
     * @var Script
     */
    private $script;
    /**
     * @var Argument[]
     */
    private $definedArguments = [];
    /**
     * @var array
     */
    private $parsedArguments = [];

    /**
     * Creates a new argument parser.
     *
     * @param Script $script
     */
    public function __construct(Script $script)
    {
        $this->script = $script;
    }

    /**
     * Adds an argument to this parser.
     *
     * @param Argument $item
     */
    public function add(Argument $item)
    {
        $this->definedArguments[] = $item;
    }

    /**
     * Returns all defined arguments.
     *
     * @return Argument[]
     */
    public function getAllDefinedArguments(): array
    {
        return $this->definedArguments;
    }

    /**
     * Returns a parsed value for the given ID.
     *
     * @param string $id
     *
     * @return string|null
     */
    public function get(string $id)
    {
        if (isset($this->parsedArguments[$id])) {
            return $this->parsedArguments[$id];
        }

        return null;
    }

    /**
     * Parses the defined arguments.
     *
     * Arguments can be skipped which is for example required if they have already been processed by an option parser.
     *
     * @param int $skipItems
     *
     * @throws TooManyArgumentsException
     */
    public function parse(int $skipItems = 0)
    {
        $this->parsedArguments = [];
        $globalArguments = $this->getScriptArguments($skipItems);

        $nGlobal = count($globalArguments);
        $nDefined = count($this->definedArguments);

        if ($nGlobal > $nDefined) {
            throw new TooManyArgumentsException(array_slice($globalArguments, $nDefined));
        }

        for ($i = 0; $i < $nDefined; $i++) {
            /* @var $argument Argument */
            $argument = $this->definedArguments[$i];
            $id = $argument->getId();

            if ($i < $nGlobal) {
                $value = $globalArguments[$i];
            } else {
                $value = $argument->getDefaultValue();
            }

            $this->parsedArguments[$id] = $value;
        }
    }

    /**
     * Returns the arguments provided by the script.
     *
     * @param int $skipItems
     *
     * @return array
     */
    private function getScriptArguments(int $skipItems): array
    {
        $arguments = $this->script->getArguments();

        // first argument will always be the script name, so we skip it
        return array_slice($arguments, $skipItems + 1);
    }
}