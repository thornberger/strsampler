<?php
declare(strict_types = 1);
namespace strsampler\Command\Argument;

use strsampler\Command\AbstractCommandItem;

/**
 * Defines a command line argument.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Argument extends AbstractCommandItem
{
    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * Creates a new command line argument.
     *
     * @param string      $id
     * @param string      $description
     * @param string|null $defaultValue
     */
    public function __construct(string $id, string $description = '', string $defaultValue = null)
    {
        parent::__construct($id, $description);

        $this->defaultValue = $defaultValue;
    }

    /**
     * Returns the defined default value of this argument.
     *
     * @return string|null
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
}