<?php

declare(strict_types = 1);
namespace strsampler\Command\Option;

/**
 * Defines a command line flag.
 *
 * A flag is an option without any values.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Flag extends Option
{

}