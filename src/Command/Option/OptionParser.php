<?php
declare(strict_types = 1);
namespace strsampler\Command\Option;

use strsampler\Common\Script;

/**
 * Parses options as --key=value or -k=value.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class OptionParser
{
    /**
     * @var Script
     */
    private $script;
    /**
     * @var Option[]
     */
    private $definedOptions = [];
    /**
     * @var array
     */
    private $parsedOptions = [];
    /**
     * @var int
     */
    private $count = 0;

    /**
     * @param Script $script
     */
    public function __construct(Script $script)
    {
        $this->script = $script;
    }

    /**
     * @param Option $item
     */
    public function add(Option $item)
    {
        $this->definedOptions[] = $item;
    }

    /**
     * @return Option[]
     */
    public function getAllDefinedOptions(): array
    {
        return $this->definedOptions;
    }

    /**
     * @param string $id
     *
     * @return string|bool|null
     */
    public function get(string $id)
    {
        if (isset($this->parsedOptions[$id])) {
            return $this->parsedOptions[$id];
        }

        return null;
    }

    /**
     * Returns the number of options read from the command line.
     *
     * @return int
     */
    public function getNumberOfReadOptions(): int
    {
        return $this->count;
    }

    /**
     * Parses the defined options.
     */
    public function parse()
    {
        $options = $this->readOptions();
        $this->countOptions($options);
        $this->parseOptions($options);
    }

    /**
     * Compiles the defined options and returns the values.
     *
     * @return array
     */
    private function readOptions(): array
    {
        $shortOptions = '';
        $longOptions = [];

        foreach ($this->definedOptions as $option) {
            $longOptions[] = $this->getOptionStringWithValueModifier($option->getId(), $option);
            $shortCut = $option->getShortCut();
            if (!empty($shortCut)) {
                $shortOptions .= $this->getOptionStringWithValueModifier($shortCut, $option);
            }
        }

        return $this->script->getOptions($shortOptions, $longOptions);
    }

    /**
     * Counts the number of read options.
     *
     * @param array $options
     */
    private function countOptions(array $options)
    {
        $this->count = count($options);
    }

    /**
     * Parses the returned values.
     *
     * @param array $options
     */
    private function parseOptions(array $options)
    {
        $this->parsedOptions = [];

        foreach ($this->definedOptions as $option) {
            /* @var $option Option */
            $id = $option->getId();
            $shortCut = $option->getShortCut();

            $value = null;
            if (isset($options[$id])) {
                $value = $options[$id];
            } elseif (isset($options[$shortCut])) {
                $value = $options[$shortCut];
            }

            $parsedValue = $this->getParsedValue($value, $option);
            if (null !== $parsedValue) {
                $this->parsedOptions[$id] = $parsedValue;
            }
        }
    }

    /**
     * Returns an option string with the value modifier needed for getopt().
     *
     * @param string $string
     * @param Option $option
     *
     * @return string
     * @see getopt()
     */
    private function getOptionStringWithValueModifier(string $string, Option $option): string
    {
        if ($option instanceof Flag) {
            return $string;
        }

        return $string.':';
    }

    /**
     * Returns the parsed value.
     *
     * @param string|null $value
     * @param Option      $option
     *
     * @return string|boolean
     */
    private function getParsedValue($value, Option $option)
    {
        if ($option instanceof Flag) {
            return null !== $value;
        }

        return $value;
    }
}