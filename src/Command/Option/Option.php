<?php

declare(strict_types = 1);
namespace strsampler\Command\Option;

use strsampler\Command\AbstractCommandItem;

/**
 * Defines a command line option.
 *
 * Options can be accessed with full or shortcut keys. The full key will be taken from the ID.
 * --id=value
 * -s=value (if s was defined as shortcut key).
 *
 * The shortcut key will only be used if the full key was not found, thus the full key overrides the shortcut key.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Option extends AbstractCommandItem
{
    /**
     * @var string
     */
    private $shortCut;

    /**
     * Creates a new option.
     *
     * @param string $id
     * @param string $description
     * @param string $shortCut
     */
    public function __construct(string $id, string $description = '', string $shortCut = '')
    {
        parent::__construct($id, $description);

        $this->shortCut = $shortCut;
    }

    /**
     * Returns the shortcut key.
     *
     * @return string
     */
    public function getShortCut(): string
    {
        return $this->shortCut;
    }
}