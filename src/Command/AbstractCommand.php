<?php

declare(strict_types = 1);
namespace strsampler\Command;

use strsampler\Command\Argument\Argument;
use strsampler\Command\Argument\ArgumentParser;
use strsampler\Command\Option\Flag;
use strsampler\Command\Option\Option;
use strsampler\Command\Option\OptionParser;

/**
 * Provides a simple command line application.
 *
 * Use the constructor to add options/flags and arguments.
 * Run the command using the run() method.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
abstract class AbstractCommand
{
    /**
     * @var OptionParser
     */
    private $optionParser;
    /**
     * @var ArgumentParser
     */
    private $argumentParser;

    /**
     * Create a new command.
     *
     * Flags/Options and Arguments should be added here.
     *
     * @param OptionParser   $optionParser
     * @param ArgumentParser $argumentParser
     */
    public function __construct(OptionParser $optionParser, ArgumentParser $argumentParser)
    {
        $this->optionParser = $optionParser;
        $this->argumentParser = $argumentParser;

        $this->addFlag(new Flag('help', 'prints help information', 'h'));
        $this->addFlag(new Flag('version', 'shows version information', 'v'));
    }

    /**
     * Run this command.
     */
    public function run()
    {
        $this->optionParser->parse();
        $skip = $this->optionParser->getNumberOfReadOptions();
        $this->argumentParser->parse($skip);

        if (true === $this->getOption('version')) {
            $this->printVersion();

            return;
        }
        if (true === $this->getOption('help')) {
            $this->printHelp();

            return;
        }

        $this->execute();
    }

    /**
     * Execute this command.
     *
     * This method has to be implemented by command line applications. It will be called by the run() method.
     */
    abstract protected function execute();

    /**
     * Returns the command name.
     *
     * @return string
     */
    abstract protected function getName(): string;

    /**
     * Returns the command description.
     *
     * @return string
     */
    abstract protected function getDescription(): string;

    /**
     * Returns the command version.
     *
     * @return string
     */
    abstract protected function getVersion(): string;

    /**
     * Adds a flag to this command.
     *
     * @param Flag $flag
     */
    protected function addFlag(Flag $flag)
    {
        $this->optionParser->add($flag);
    }

    /**
     * Adds an option to this command.
     *
     * @param Option $option
     */
    protected function addOption(Option $option)
    {
        $this->optionParser->add($option);
    }

    /**
     * Adds an argument to this command.
     *
     * Arguments will be evaluated in the order added to the command.
     *
     * @param Argument $argument
     */
    protected function addArgument(Argument $argument)
    {
        $this->argumentParser->add($argument);
    }

    /**
     * Returns an argument value for a given argument ID.
     *
     * @param string $id
     *
     * @return string|null
     */
    protected function getArgument(string $id)
    {
        return $this->argumentParser->get($id);
    }

    /**
     * Returns an option value for a given option ID.
     *
     * @param string $id
     *
     * @return string|null
     */
    protected function getOption(string $id)
    {
        return $this->optionParser->get($id);
    }

    /**
     * Returns an flag value for a given flag/option ID.
     *
     * This method is an alias for getOption($id).
     *
     * @param string $id
     *
     * @return boolean
     */
    protected function getFlag(string $id)
    {
        return $this->optionParser->get($id);
    }

    /**
     * Prints version information.
     */
    protected function printVersion()
    {
        printf("%s %s\n", $this->getName(), $this->getVersion());
    }

    /**
     * Prints help information.
     */
    protected function printHelp()
    {
        printf("%s %s -- %s\n", $this->getName(), $this->getVersion(), $this->getDescription());
        print("\nUsage:\n");
        printf('%s ', $this->getName());
        $this->printOptionsUsage();
        $this->printArgumentsUsage();

        print("\n\nOptions:\n");
        $this->printOptionsDescription();
        print("\nArguments:\n");
        $this->printArgumentsDescription();
        print("\n\n");
    }

    /**
     * Prints option usages.
     */
    private function printOptionsUsage()
    {
        foreach ($this->optionParser->getAllDefinedOptions() as $option) {
            $id = $option->getId();
            $suffix = '';
            if (!$option instanceof Flag) {
                $suffix .= sprintf('=<%s>', $option->getId());
            }
            $longOption = '--'.$id.$suffix;

            $shortOption = '';
            $shortCut = $option->getShortCut();
            if (!empty($shortCut)) {
                $shortOption .= '|-'.$shortCut.$suffix;
            }

            $string = $longOption.$shortOption;

            printf('[%s]', $string);

        }
    }

    /**
     * Prints argument usages.
     */
    private function printArgumentsUsage()
    {
        foreach ($this->argumentParser->getAllDefinedArguments() as $argument) {
            printf(' <%s>', $argument->getId());
        }
    }

    /**
     * Prints options descriptions.
     */
    private function printOptionsDescription()
    {
        foreach ($this->optionParser->getAllDefinedOptions() as $option) {
            printf("%s: %s\n", $option->getId(), $option->getDescription());
        }
    }

    /**
     * Prints argument descriptions.
     */
    private function printArgumentsDescription()
    {
        foreach ($this->argumentParser->getAllDefinedArguments() as $argument) {
            printf("%s: %s\n", $argument->getId(), $argument->getDescription());
        }
    }
}