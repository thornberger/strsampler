<?php

declare(strict_types = 1);
namespace strsampler\Command;

use strsampler\Common\DescribableInterface;
use strsampler\Common\IdentifiableInterface;

/**
 * Holds information about command items such as options, flags or arguments.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
abstract class AbstractCommandItem implements IdentifiableInterface, DescribableInterface
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $description;

    /**
     * Creates a new command item using the provided ID and description.
     *
     * @param string $id
     * @param string $description
     */
    public function __construct(string $id, string $description)
    {
        $this->id = $id;
        $this->description = $description;
    }

    /**
     * Returns the command item ID.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Returns the command item description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}