<?php

declare(strict_types = 1);
namespace strsampler\Benchmark;

/**
 * Provides access to benchmarking information.
 *
 * @author Tobias Hornberger <tobias.hornberger@falsemirror.de>
 */
class Benchmark
{
    /**
     * Returns the peak memory allocated by the script.
     *
     * This wraps the native memory_get_peak_usage function.
     *
     * @return int
     */
    public function getPeakMemoryUsage(): int
    {
        return memory_get_peak_usage(false);
    }
}