**strsampler 1.0.0**

strsampler is a command line utility for sampling byte streams of unknown length. Streams can be provided from stdin, from files, URLs, or generated randomly. Samples will have a length of the provided sample size or the length of the input if it is shorter than the desired sample size.

# Installation #
strsampler uses the composer package manager to handle its dependencies. Simply run composer install from the root directory.

`php composer.phar install`

This will install all required dependencies and setup the autoloader.

# Usage #
After installing strsampler use the command in *bin*.

`php bin/strsampler`

`php bin/strsampler --version` will show the version information.

`php bin/strsampler --help` will display a synoptic overview of all parameters.

All other modes of operation will take two parameters: the sample size (default: 5) and some input which is depending on the selected *source* option:

`php bin/strsampler --source=<source> <sampleSize> <input>`


## Source Types ##
The source type defines which data strsampler will use.

### stdin ###

`php bin/strsampler --source=stdin 5`

or

`php bin/strsampler 5`

*stdin* (standard input) is the default source type. It will take all strings piped to strsampler.
Notice: *stdin* input will ignore all new line characters.

### url ###

`php bin/strsampler --source=url 5 "https://www.random.org/strings/?num=162&len=1&digits=on&upperalpha=on&loweralpha=on&format=plain"`

`php bin/strsampler --source=url 5 "/path/to/my/file"`

*url* reads data from a URL or file path. The data is expected to be provided in a raw format with one character per line.

### string ###
`php bin/strsampler --source=string 5 "hello world"`

*string* takes a simple string as input parameter.

### random ###
`php bin/strsampler --source=random 5 100`

*random* creates an arbitrary number of cryptographically secure random bytes.

## Output ##
The output will be provided in two lines with the sample and the maximum allocated memory.

    Random Sample: hello
    Maximum allocated memory during runtime: 891 kB


**Important:**
If the sample contains unprintable characters they will be replaced by spaces in the output. However unprintable characters will in general be sampled the same way as printable characters, with the aforementioned exception of the ignored new lines for the *stdin* source type.


## Algorithms ##
The sampling algorithm can be chosen with the *algorithm* option.

`php bin/strsampler --algorithm=algorithmR 5`

strsampler 1.0.0 will only be shipped with "algorithmR" introduced by Jeffrey Vitter in [1]. Further algorithms will be added in the future.

# Extending strsampler #
strsampler can be easily extended with new stream sources and sampling algorithms. In order to create a new sampling algorithm implement the `SamplingStrategyInterface`and place it in `src/Sampler/Strategy`. Register the new strategy in `src/strsampler.php`.
If you want to create a new stream source, implement the `StreamInterface` and `StreamFactoryInterface` for a stream and its factory and register the factory in `src/strsampler.php`.

Make sure to run and extend unit and integration tests. Unit tests can be found in `src/Tests/Unit`, integration tests in `src/Tests/Integration`. Both are based on PHPUnit which is installed as composer dependency.


# Contact #
Tobias Hornberger
[tobias.hornberger@falsemirror.de]


# Sources #
[1] Jeffrey Scott Vitter: Random Sampling with a Reservoir http://www.cs.umd.edu/~samir/498/vitter.pdf




[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/thornberger/strsampler/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/thornberger/strsampler/?branch=master)

[![Code Coverage](https://scrutinizer-ci.com/b/thornberger/strsampler/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/thornberger/strsampler/?branch=master)